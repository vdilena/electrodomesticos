<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ElectrodomesticosController@listar');

Route::post('guardarContacto', ['as' => 'guardarContacto', 'uses' => 'ElectrodomesticosController@guardar']);

Route::get('contactos', 'ElectrodomesticosController@listarContactos');